# BACKEND STARTER

![alt tag](https://img.shields.io/badge/Poweredby-WEBARQ-blue.svg) ![alt tag](https://img.shields.io/badge/Developer-Muhamad Reza AR-green.svg)

### How to Install


A. Clone the project

```sh
	
	git clone git@dev.webarq.info:webarq/reza.git

```

B. Setting Database Connection in .env file

```sh
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=databaseName
	DB_USERNAME=username
	DB_PASSWORD=passwordname

```

C. composer install

D. Run Webarq Console

```sh

php artisan webarq:update

```

Finish


Login User

|  Username  |      Password      |  Role |
|:--------:|:-------------:|------:|
|superadmin |  webarq | Super Admin |


